<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/23cbbbdcb5.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/new-music.css">
    <title>Music</title>
</head>

<body>
    <?php
    $collectionOne = array(
        array("Easy On Me", "Adele", "https://www.youtube.com/watch?v=U3ASj1L6_sY", "./image/easy_on_me.jpeg"),
        array("Shivers", "Ed Sheeran", "https://www.youtube.com/watch?v=Il0S8BoucSA", "./image/shiver.jpeg"),
        array("Pushin P", "Guna", "https://www.youtube.com/watch?v=9g08kucPQtE", "./image/Guna.jpeg"),
        array("Need To Know", "Doja Cat", "https://www.youtube.com/watch?v=dI3xkL7qUAc", "./image/docat.jpeg"),
        array("Bad Habbits", "Ed Sheeran", "https://www.youtube.com/watch?v=orJSJGHjBLI", "./image/bad_habit.jpeg"),
        array("Ghost", "Justin Bieber", "https://www.youtube.com/watch?v=Fp8msa5uYsc", "./image/ghost.jpeg"),
        array("Heat Waves", "Glass Animals", "https://www.youtube.com/watch?v=mRD0-GxqHVo", "./image/heat_waves.jpeg"),
        array("Stay", "The Kid LAROI & Justin Beiber", "https://www.youtube.com/watch?v=kTJczUoc26U", "./image/stay.jpeg"),
        array("Super Gremlin", "Kodak Black", "https://www.youtube.com/watch?v=kiB9qk4gnt4", "./image/super.jpeg"),
        array("Surface Pressure", "Jessica Darrow", "https://www.youtube.com/watch?v=tQwVKr8rCYw", "./image/surface.jpeg"),
    );
    ?>
    <div style="background-color:#110f16">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xl-7">
                    <article class="c-article">
                        <header class="c-article__header">
                            <h1 class="c-article__title">
                                <span class="u-font-lora">DEvops is the art which is most nigh to tears and memory.</span>
                            </h1>
                        </header>
                    </article>
                </div>
                <div class="col-lg-12 col-xl-5 second-col">
                    <img src="./image/girl-listening-music-on-sofa (1).png" alt="people are listening to music" width="100%" height="100%" />
                </div>
            </div>
        </div>
    </div>
    <section class="dark">
        <div class="container py-4">
            <h1 class="h1 text-center" id="pageHeaderTitle">My Playlist</h1>
            <?php
            shuffle($collectionOne);
            foreach ($collectionOne as $album) {
                echo (" <article class='postcard dark blue'>
        <a class='postcard__img_link' href=$album[2]>
            <img class='postcard__img' src=$album[3] alt='Image Title' />
        </a>
        <div class='postcard__text'>
            <h1 class='postcard__title blue'><a href=$album[2]>$album[0]</a></h1>
            <div class='postcard__subtitle small'>
                <time datetime='2020-05-25 12:00:00'>
                    <i class='fas fa-calendar-alt mr-2'></i>Mon, May 25th 2020
                </time>
            </div>
            <div class='postcard__bar'></div>
            <div class='postcard__preview-txt'>Artist: $album[1]</div>
            <ul class='postcard__tagbox'>
                <li class='tag__item'><i class='fas fa-tag mr-2'></i>Popular</li>
                <li class='tag__item play blue'>
                    <a href=$album[2]><i class='fas fa-play mr-2'></i>Play </a>
                </li>
            </ul>
        </div>
    </article>");
            }
            ?>
        </div>
    </section>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous"></script>
</body>

</html>